package Lab2_SE;
import java.util.Random;

public class BubbleSort {

    public static void main( String[] args ){
        Random rnd = new Random();
        int v[] = new int[10];
        for ( int i = 0; i<=9 ; i++)
            v[ i ] = rnd.nextInt(10) + 1;

        //BubbleSort
        System.out.println( "Sorted vector using bubblesorting method: " );
        for( int i=0 ; i<9 ; i++)
            for( int j=i+1 ; j<=9 ; j++)
                if( v[ i ] > v[ j ]){
                    int aux = v[ i ];
                    v[ i ] = v [ j ];
                    v[ j ] = aux;
                }
        for (int i = 0 ; i <= 9 ; i++)
            System.out.println( v[ i ]+ " " );

    }
}
