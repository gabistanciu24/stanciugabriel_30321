package Lab2_SE;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        int number = (int)(Math.random() * 100);
        // System.out.println("The number is " + number);

        Scanner in = new Scanner(System.in);
        int guess = -1;
        int k=0;

        while (guess != number) {
            System.out.println("Guess a magic number between 0 and 100");
            guess = in.nextInt();

            if (guess == number)
                System.out.print("Yes, the number is " + number);
            else if (guess > number) {
                System.out.println("Wrong answer, number is too high");
                k++;
            } else {
                System.out.println("Wrong answer, number is too low");
                k++;
            }
            if( k==3){
                System.out.println( "You lost");
                break;
            }
        }

    }
}