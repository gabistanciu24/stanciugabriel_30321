package Lab2_SE;
import java.util.Scanner;

public class factorialFunctions {

    public static long factorial( int n ){
        long fact=1;
        for( int i = 2 ; i <= n ; i++ )
            fact *= i;
        return fact;
    }

    public static long recursFactorial( int n ){
        if ( n> 1)
            return n * recursFactorial( n - 1);
        else
            return 1;
    }

    public static void main( String[] args ){
        Scanner in = new Scanner( System.in );
        System.out.println( "Number: " );
        int n = in.nextInt();
        System.out.println( "The factorial of " +n+ "using recursive method is " +recursFactorial(n));
        System.out.println( "The factorial of " +n+ "using non-recursive method is " +factorial(n));
    }
}
