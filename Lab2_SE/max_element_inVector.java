package Lab2_SE;
import java.util.Scanner;

public class max_element_inVector {
    public static void main( String[] args ){
        Scanner in = new Scanner( System.in );
        System.out.println( "Type in the number of the vector elements ");
        int n = in.nextInt();

        int v[ ] = new int[ n ];
        for( int i = 0 ; i < n ; i++ ){
            System.out.println( " Element: " );
            v[i] = in.nextInt();
        }

        int maximum = v[0];
        for( int i = 1; i < n ; i++)
            if( v[ i ] > maximum )
                maximum = v[ i ];
        System.out.println( "Maximum element in vector is: " +maximum );
    }
}
