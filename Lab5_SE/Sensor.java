package Lab5_SE;

public abstract class Sensor {
    public String location;
    public abstract int readValue();
    public String getLocation(){
        return location;
    }
}
