package Lab5_SE;

public class Rectangle extends Shape{
    private double width;
    private double length;

    Rectangle(){
        super();
        width=1.0;
        length=2.0;
    }

    Rectangle(double width, double length, String color, boolean filled){
        this.width=width;
        this.length=length;
        this.color=color;
        this.filled=filled;
    }

    Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width){
        this.width=width;
    }

    public double getLength(){
        return length;
    }

    public void setLength(double length){
        this.length=length;
    }

    @Override
    public double getArea(){
        return length*width;
    }

    @Override
    public double getPerimeter(){
        return 2*(width+length);
    }

    @Override
    public String toString(){
        return "Rectangle with width=" +width+ ", and length= "+length+", a subclass of "+super.toString();
    }
}
