package Lab5_SE;

public class Square extends Rectangle{
    Square(){
        super();
    }
     public Square(double side){
        super(side,side);
     }

     public Square(double side,String color,boolean filled){
        super(side,side,color,filled);
     }

     public double getSide(){
        return super.getLength();
     }

     public void setSide(double side){
        this.setLength(side);
        this.setWidth(side);
     }

    @Override
    public String toString(){
        return "Square was created with side= " + super.getLength() +", a subclass of "+ super.toString();
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }
}
