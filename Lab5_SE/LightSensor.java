package Lab5_SE;
import java.util.Random;

public class LightSensor extends Sensor{
    Random light=new Random();

    @Override
    public int readValue() {
        return light.nextInt(100);
    }
}
