package Lab5_SE;
//You should create a class RotatedImage that implements Image, in the ProxyImage, when the displayRealImage is false, you call the display of the RotatedImageclass

public class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;
    private boolean displayRealImage;

    public ProxyImage(String fileName,boolean displayRealImage){
        this.fileName = fileName;
        this.displayRealImage=displayRealImage;
    }

    @Override
    public void display(){
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        if(displayRealImage)
            realImage.display();
        else
            RotatedImage();
    }

    @Override
    public void RotatedImage(){
        System.out.println("Display rotated "+fileName);
    }
}
