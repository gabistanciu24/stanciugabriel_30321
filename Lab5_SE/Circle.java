package Lab5_SE;

public class Circle extends Shape{
    protected double radius;

    Circle(){
        super();
        radius=1.0;
    }
    Circle(double radius){
        super();
        this.radius=radius;
    }

    Circle(double radius, String color, boolean filled){
        super();
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius){
        this.radius=radius;
    }

    @Override
    public double getArea(){
        return Math.PI * (radius * radius);
    }

    @Override
    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString(){
        return "Circle with radius= " +radius+", a subclass of "+ super.toString();
    }
}
