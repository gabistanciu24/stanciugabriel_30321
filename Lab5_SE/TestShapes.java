package Lab5_SE;

public class TestShapes {
    public static void main(String[] args){
        Shape[] shapes = new Shape[3];

        shapes[0]=new Circle(3);
        shapes[1]=new Rectangle(1.2,2.2);
        shapes[2]=new Square(3);

        for(int i=0;i<3;i++){
            System.out.println("Shape "+i+ " has area= "+ shapes[i].getArea() + " and perimeter= "+shapes[i].getPerimeter());
        }
    }
}
