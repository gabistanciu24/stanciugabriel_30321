package Lab5_SE;
import java.util.concurrent.TimeUnit;

public class Controller {
    TemperatureSensor temp = new TemperatureSensor();
    LightSensor lght = new LightSensor();

    private static volatile Controller controller =null;

    private Controller(){}

    public static Controller getInstance(){
        synchronized (Controller.class){
            if(controller == null){
                controller = new Controller();
            }
        }
        return controller;
    }

    public void Control() {
        for (int i = 1; i <= 20; i++) {
            System.out.println("The temperature at second " + i + " is " + temp.readValue() + " and the light is " + lght.readValue());
        }
    }
}
