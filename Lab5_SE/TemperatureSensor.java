package Lab5_SE;
import java.util.Random;

public class TemperatureSensor extends Sensor{
    Random temperature = new Random();

    @Override
    public int readValue() {
        return temperature.nextInt(100);
    }
}
