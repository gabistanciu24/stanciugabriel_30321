package Lab3_SE;

public class Author {
    private String name;

    public String getName( ){
        return name;
    }

    private String email;

    public String getEmail( ){
        return email;
    }

    public void setEmail( String email ){
        this.email = email;
    }

    private char gender;

    public Author( String name, String email, char gender ){
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String toString( ){
        return name+ "(" + gender + ")" + " at " + email;
    }
}
