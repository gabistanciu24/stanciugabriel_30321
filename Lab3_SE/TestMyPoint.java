package Lab3_SE;

public class TestMyPoint {
    public static void main( String[] args ){
        MyPoint p = new MyPoint( );
        System.out.println( "Point created using default constructor. x= " + p.getX() + " and y= " + p.getY( ) );

        p.setY(1);
        p.setX(1);

        System.out.println("The point changed to (1, 1).x = " +p.getX() + " and y = " + p.getY( ) );

        MyPoint p2=new MyPoint( 1,1 );
        System.out.println("Point created using parametrized constructor. Point is "+ p2.toString());

        p2.setXY( 2,2 );
        System.out.println("The point changed to( 2, 2). Point is: "+p2.toString( ) );

        System.out.println("The distance between point ( 2, 2) and the point ( 1, 1) is: " + p2.distance( 1, 1 ) );
        System.out.println("The distance between point ( 2, 2) and the first point ( 1, 1) is: " + p2.distance( p ) );
    }
}
