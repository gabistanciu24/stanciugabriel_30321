package Lab3_SE;

public class Flower {
    static int numberOf_Flowers = 0;
    int petal;

    Flower( ){
        System.out.println( "Flower created!" );
        numberOf_Flowers++;
    }

    public static void main( String[ ] args ){
        Flower[] garden = new Flower[5];
        for( int i = 0 ; i < 5 ; i++ ){
            Flower f = new Flower();
            garden[ i ] = f;
        }
        System.out.println( "Instances: " + getNumberOfFlowers( ) );
    }

    public static int getNumberOfFlowers( ){
        return numberOf_Flowers;
    }
}
