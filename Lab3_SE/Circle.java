package Lab3_SE;

public class Circle {
    private double rad=1.0;
    private String color="red";

    public Circle(double r, String col){
        rad=r;
        color=col;
    }

    public double getRadius(){
        return rad;
    }
    public double getArea(){
        return Math.PI*(rad*rad);
    }
}
