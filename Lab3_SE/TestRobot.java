package Lab3_SE;

public class TestRobot {
    public static void main( String[] args ){
        Robot robot = new Robot();
        System.out.println( "Robot position: " +robot.toString( ) );
        robot.changepos( 8 );
        System.out.println( "Robot moved" );
        System.out.println( "Robot position: " +robot.toString( ) );
    }
}
