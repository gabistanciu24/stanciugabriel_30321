package Lab4_SE;

public class TestCylinder {
    public static void main(String[] args){
        Cylinder cylinder= new Cylinder(2.0,3.0);
        System.out.println("Cylinder was created, our cylinder's radius is: "+cylinder.getRadius()+"\nIts Area is: "+cylinder.getArea()+"\nIts Height is: "+cylinder.getHeight()+"\nAnd its Volume is: "+cylinder.getVolume());
    }
}
