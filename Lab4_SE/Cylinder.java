package Lab4_SE;
import Lab4_SE.Circle;

public class Cylinder extends Circle{
    double height=1.0;
    Cylinder(){
        super(1,"red");
    }
    public Cylinder(double radius){
        super( radius, "red");
    }

    public Cylinder( double radius, double height ){
        super(radius, "red");
        this.height = height;
    }
    public double getHeight( ){
        return height;
    }
   public double getVolume( ){
        return super.getArea( )*height;
    }
    @Override //alawys write it when overriding methods from the base class and when overriding the toString
    public double getArea(){
        return 2*Math.PI*this.getRadius()*(this.getRadius()*height);
    }


}
