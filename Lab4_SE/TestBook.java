package Lab4_SE;


import java.util.Arrays;
import java.util.Scanner;
public class TestBook {
    public static void main( String[ ] args ){

        Scanner keyboard = new Scanner( System.in );
        System.out.println( "Type in the number of authors: " );
        int n = keyboard.nextInt( );

        Author[ ] authors = new Author[ n ];

        for( int i = 1 ; i <= n ; i++ )
        {
            authors[ i - 1 ] = new Author( "Author:"+ i, "" , i % 2 == 0 ? 'M' : 'F' );
        }
        Book book=new Book( "Marry Jane", authors, 30.20 );
        book.setQtyInStock( 30 );
        System.out.println( "Book was created.\nTitle: "+ book.getName()+ " \nAuthors: "+ Arrays.toString(book.getAuthors())+ "\nPrice: $"+book.getPrice()+ "\nQuantity: "+book.getQtyInStock());
        System.out.println( "\n" +book) ;
        book.printAuthors( );
    }
}
